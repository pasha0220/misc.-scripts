tmp=`mktemp -u`
notUtffiles=$(find . -regex '.*\.\(cpp\|h\|\hpp\|c\)' -exec file --mime-encoding {} \; | grep -v utf-8 | cut -d ":" -f1); \
for ff in $notUtffiles; \
do \
 if [ -f $(readlink -e "$(pwd)/$ff") ] ; \
 then \
  echo "$ff"; \
  iconv -f windows-1251 -t utf-8 "$ff" > $tmp; \
  mv $tmp "$ff"; \
 fi; \
done